import React from 'react';
import { View, Text, Button, AsyncStorage, ToastAndroid, ActivityIndicator } from 'react-native';
import globalTheme from '../style/GlobalTheme'
import { MaterialCommunityIcons } from 'react-native-vector-icons';
import axios from 'axios'
import WeatherList from './WeatherList'



export default class Home extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            city: '',
            name: '',
            checked: 'daily',
            loading: true,
            report: null,
            error: false
        }
        setTimeout(() => {this.fetchWeather()}, 1000);
    }
    retrieveData = async () => {
        this.setState({loading: true, report: null});

        try {
          const city = await AsyncStorage.getItem('CITY');
          const name = await AsyncStorage.getItem('NAME');
          const checked = await AsyncStorage.getItem('CHECKED');
          this.setState({city, name, checked});
        } catch (error) {
            this.setState({city: '', name: '', checked: 'daily'});
            // Error retrieving data
        }
        if (this.state.city) {
            ToastAndroid.show('Fetch weather from ' + this.state.city, ToastAndroid.SHORT);
        } else {
            ToastAndroid.show('Go to preferencies and indicate you city', ToastAndroid.SHORT);
        }
        setTimeout(() => {this.fetchWeather()}, 1000);
    }
    
    componentDidMount() {
        this.retrieveData()
    }

    fetchWeather() {
        axios.get(
            'http://api.openweathermap.org/data/2.5/forecast?q=' + 
            this.state.city + 
            (this.state.checked === 'daily' ? '&cnt=7' : '' )+ '&mode=json&units=metric&appid=9e930c366f06818fb6684b25030377ec'
        )
        .then((response) => {
            this.setState({report: response.data, error: false})
        }).catch((error) => {
            if (error.message == 'Request failed with status code 404') {
                this.setState({error: true})
            }
        })
        this.setState({loading: false})
    }

    render() {
        let loader =  this.state.loading ? 
        <ActivityIndicator 
            color={globalTheme.button.backgroundColor}
            size="large"
        /> : 
        <Text/>
        let header = this.state.error ? 
        <Text style={globalTheme.error}>*The city {this.state.city} does not exist*</Text> :
        this.state.name ? 
        <Text>Hello {this.state.name}, 
            here is {this.state.checked == 'daily' ? 'the daily weather ' : 'a 5 days forecast '} 
            in the city of {this.state.city}
        </Text> :
        <Text>Hello, you should go to settings to set preferencies</Text>
      

            
        if (!this.state.report) {
            return (
                <View style={globalTheme.container}>
                    <View>
                        <Button color={globalTheme.button.backgroundColor}  onPress={() => this.retrieveData()} title="Refresh"/>
                        {header}
                    </View>
                    {loader}
                </View>
            )
        } else {
            return (
                <View>
                <View style={globalTheme.container}>
                    <View>
                        <Button color={globalTheme.button.backgroundColor}  onPress={() => this.retrieveData()} title="Refresh"/>
                        {header}
                    </View>
                </View>
                    <View>
                        <WeatherList navigator={this.props.navigation} city={this.state.city} forecast={this.state.checked} data={this.state.report.list} />
                    </View>    
                </View>            
            )
        }
    }
}