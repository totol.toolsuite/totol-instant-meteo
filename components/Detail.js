import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import globalTheme from '../style/GlobalTheme'
import moment from 'moment';

export default class Detail extends React.Component {
    render() {
        const item = this.props.route.params.item
        console.log(item)
        const iconWeather = 
            item.weather[0].main.toLowerCase() === 'clouds' ? 
            require('../assets/weather-icons/cloud.png') :
            item.weather[0].main.toLowerCase() === 'rain' ?
            require('../assets/weather-icons/rain.png') :
            require('../assets/weather-icons/sunny.png')
        const temperature = 
            <Image 
                source={require('../assets/weather-icons/temperature.png')}
                style={{width: 50, height: 50}}
            />
        const infos = 
            <Image 
                source={require('../assets/weather-icons/info.png')}
                style={{width: 50, height: 50}}
            />
        const humidity = 
            <Image 
                source={require('../assets/weather-icons/percent.png')}
                style={{width: 50, height: 50}}
            />
        const wind = 
            <Image 
                source={require('../assets/weather-icons/wind.png')}
                style={{width: 50, height: 50}}
            />
        const winddir = 
            <Image 
                source={require('../assets/weather-icons/compass.png')}
                style={{width: 50, height: 50}}
            />
        const rain = 
            <Image 
                source={require('../assets/weather-icons/umbrella.png')}
                style={{width: 50, height: 50}}
            />
        const cloudiness = 
            <Image 
                source={require('../assets/weather-icons/cloudiness.png')}
                style={{width: 50, height: 50}}
            />
        const winddirfun = (degree) => {
            let workedDegree = degree - 90
            return (
                <Image 
                    source={require('../assets/weather-icons/winddir.png')}
                    style={{width: 30, height: 30, transform: [{ rotate: workedDegree+'deg' }]}}
                />
            )
        } 
        
        const tempColor = (temp) => {
            return (
                temp < 10 ? style.lightblueText : 
                temp < 20 ? style.orangeText :
                style.redText
            )
        }        
        
        const humidityColor = (hum) => {
            return (
                hum < 25 ? style.greenText : 
                hum < 60 ? style.orangeText :
                style.redText
            )
        }

        const windColor = (wind) => {
            return (
                wind < 25 ? style.greenText : 
                wind < 50 ? style.orangeText :
                style.redText
            )
        }

        const rainColor = (rain) => {
            return (
                rain < 24 ? style.greenText : 
                rain < 56? style.orangeText :
                style.redText
            )
        }        
        
        const cloudinessColor = (cloud) => {
            return (
                cloud < 10 ? style.greenText : 
                cloud < 40 ? style.orangeText :
                style.redText
            )
        }

        const details = 
        <View style={[globalTheme.buttonSecondary]}>
            <View style={[style.inline, style.bigMargins]}>
                <Image 
                    source={iconWeather}
                    style={{width: 100, height: 100}}
                />
                <View style={{marginLeft:15}}>
                    <Text style={[style.whiteText, style.bigText]}>
                        {this.props.route.params.city}
                    </Text>
                    <Text style={[style.whiteText, style.semiBigText]}>
                        {moment(item.dt * 1000).format('DD-MM-YYYY HH:mm')}
                    </Text>
                </View>
            </View>
            <View style={[style.border], {marginVertical: 10}}>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {infos}
                    <Text style={[style.whiteText, style.lilText]}>
                        {item.weather[0].description.charAt(0).toUpperCase() + 
                        item.weather[0].description.slice(1)}
                    </Text>
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {temperature}
                    <Text style={[tempColor(item.main.temp), style.semiBigText]}>{Math.round(item.main.temp)}°C</Text>
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {humidity}
                    <Text style={[(item.main.humidity? humidityColor(item.main.humidity) : ''), style.semiBigText]}>{Math.round(item.main.humidity)}%</Text>
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {wind}
                    <Text style={[item.wind.speed ? windColor(item.wind.speed*3.6) : '', style.semiBigText]}>{Math.round(item.wind.speed * 3.6)}km/h</Text>
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {winddir}
                    <Text>                              </Text>
                    <Text style={[style.whiteText, style.semiBigText]}>{item.wind.deg}°</Text>
                    {item.wind ? winddirfun(item.wind.deg) : <Text/>}
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {rain}
                    <Text style={[(item.rain ? rainColor(item.rain["3h"]*8) : style.whiteText), style.semiBigText]}>{item.rain && item.rain["3h"] ? Math.round(item.rain["3h"]*8) + "mm/d": "No rain"}</Text>
                </View>
                <View style={[style.secondBackground, {padding: 15}, style.inline, style.lilMargins]}>
                    {cloudiness}
                    <Text style={[(item.clouds ? cloudinessColor(item.clouds.all) : style.whiteText), style.semiBigText]}>{item.clouds && item.clouds["all"] ? Math.round(item.clouds.all) + "%": "No cloud"}</Text>
                </View>
                <View style={[style.secondBackground, {paddingVertical: 60}]}></View>
            </View>
        </View>
        return (
            <View>
                {details}
            </View>
        )
    }
}

const style = StyleSheet.create({
    whiteText: {
        color: '#FFF',
        justifyContent: 'center',
    },
    orangeText: {
        color: 'orange',
        justifyContent: 'center',
    },
    redText: {
        color: 'red',
        justifyContent: 'center',
    },
    lightblueText: {
        color : '#14fbff',
        justifyContent: 'center'
    },
    yellowText: {
        color : 'yellow',
        justifyContent: 'center'
    },
    greenText: {
        color: 'green',
        justifyContent: 'center',
    },
    border: {
        borderWidth: 1,
        borderBottomColor: 1,
        borderBottomColor: '#202340',
    },
    bold: {
        fontWeight: 'bold',
        marginHorizontal:5
    },
    background: {
        backgroundColor : '#e54b65',
    },
    secondBackground: {
        backgroundColor : '#364670'
    },
    bigText: {
        fontSize : 50,
    },    semiBigText: {
        fontSize : 25,
    },
    lilText: {
        fontSize : 20,
    },
    bigMargins: {
        marginVertical:100,
        marginHorizontal:50,
    },
    lilMargins: {
        paddingVertical:35,
        paddingHorizontal:60,
    },
    inline: {
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center'
    },
    temp: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 22
    }
})