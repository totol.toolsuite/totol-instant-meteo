import React from 'react';
import { TextInput, View, Button } from 'react-native';
import globalTheme from '../style/GlobalTheme'
import { createStackNavigator } from '@react-navigation/stack'
import Search from './Search';
import Result from './Result';
import Detail from './Detail';
export default class Weather extends React.Component {
    render() {
        return (
            <Stack.Navigator
            screenOptions={
                {headerStyle:globalTheme.button,
                headerTitleStyle:{color:'white'}}
            }>
                <Stack.Screen name="Search" component={Search} />
                <Stack.Screen name="Result" component={Result} 
                    options={ 
                        ({route})=>({
                            title:"Result for: " + 
                            route.params.city + 
                            (route.params.country ? '(' + route.params.country +')' : '')
                        })
                }/>
                <Stack.Screen name="Details" component={Detail} />
            </Stack.Navigator>
        )
    }
}

const Stack = createStackNavigator()