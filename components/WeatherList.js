import React from 'react';
import { FlatList, Text, View, StyleSheet, Image } from 'react-native';
import moment from 'moment';
import globalTheme from '../style/GlobalTheme'
import _ from 'lodash';
import FadeInView from '../animation/fadeInView'

export default class WeatherList extends React.Component {
    goToDetails = (item) => {
        this.props.navigator.navigate('Details', {item, city:this.props.city})
    }

    timestampToDay = (day) => {
        return moment(day * 1000).format("dddd");
    }
    filterArray () {
        if (this.props.forecast === 'forecast') {
            let returnedData = [];
            for (let data of this.props.data) {
                let dataTime = parseInt(moment(data.dt * 1000).format('H'), 10);
                if (dataTime >= 6 && dataTime <= 8) {
                    returnedData.push(data);
                } else if (dataTime >= 14 && dataTime <= 16) {
                    returnedData.push(data);
                }
            }
            return returnedData
        } else {
            return this.props.data;
        }
    }
    render() {
        const forecast = ({ item, index }) => 
        index === 0 ? 
        (
        <FadeInView delay={index * 50}>
            <View style={[style.view, style.firstView]}
                onTouchEnd={() => this.goToDetails(item)}>
                <Image 
                    source={
                        item.weather[0].main.toLowerCase() === 'clouds' ? 
                        require('../assets/weather-icons/cloud.png') :
                        item.weather[0].main.toLowerCase() === 'rain' ?
                        require('../assets/weather-icons/rain.png') :
                        require('../assets/weather-icons/sunny.png')
                    }
                    style={{width: 100, height: 100}}
                />
                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                    <Text style={[style.white, style.bold]}>
                        {this.timestampToDay(item.dt)}
                    </Text>
                    <Text>
                        {moment(item.dt * 1000).format('H') <= 8 && moment(item.dt * 1000).format('H') >= 6 ? 'Morning' : 'Afternoon'}
                    </Text>
                </View>
                
                <Text style={[style.temp, style.bigText]}>
                    {Math.round(item.main.temp)} °C
                </Text>
            </View>
        </FadeInView>
        ) : 
        <FadeInView delay={index * 50}>
            <View style={style.view}
                onTouchEnd={() => this.goToDetails(item)}>
                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                    <Image 
                        source={
                            item.weather[0].main.toLowerCase() === 'clouds' ? 
                            require('../assets/weather-icons/cloud.png') :
                            item.weather[0].main.toLowerCase() === 'rain' ?
                            require('../assets/weather-icons/rain.png') :
                            require('../assets/weather-icons/sunny.png')
                        }
                        style={{width: 50, height: 50}}
                    />
                    <Text style={[style.white, style.bold]}>
                        {this.timestampToDay(item.dt)}
                    </Text>
                    <Text>
                        {moment(item.dt * 1000).format('H') <= 8 && moment(item.dt * 1000).format('H') >= 6 ? 'Morning' : 'Afternoon'}
                    </Text>
                </View>
                
                <Text style={style.temp}>
                    {Math.round(item.main.temp)} °C
                </Text>
            </View>
        </FadeInView>

        const daily = ({ item, index }) => 
        index === 0 ? 
        (
        <FadeInView delay={index * 50}>
            <View style={[style.view, style.firstView]}
                onTouchEnd={() => this.goToDetails(item)}>
                <Image 
                    source={
                        item.weather[0].main.toLowerCase() === 'clouds' ? 
                        require('../assets/weather-icons/cloud.png') :
                        item.weather[0].main.toLowerCase() === 'rain' ?
                        require('../assets/weather-icons/rain.png') :
                        require('../assets/weather-icons/sunny.png')
                    }
                    style={{width: 100, height: 100}}
                />
                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                    <Text style={[style.white, style.bold]}>
                        {this.timestampToDay(item.dt)}
                    </Text>
                    <Text>
                        {moment(item.dt * 1000).format('HH:mm')}
                    </Text>
                </View>
                
                <Text style={[style.temp, style.bigText]}>
                    {Math.round(item.main.temp)} °C
                </Text>
            </View>
        </FadeInView>
        ) : 
        <FadeInView delay={index * 50}>
            <View style={style.view}
                onTouchEnd={() => this.goToDetails(item)}>
                <View style={{flex:1, flexDirection:'row', alignItems:'center'}}>
                    <Image 
                        source={
                            item.weather[0].main.toLowerCase() === 'clouds' ? 
                            require('../assets/weather-icons/cloud.png') :
                            item.weather[0].main.toLowerCase() === 'rain' ?
                            require('../assets/weather-icons/rain.png') :
                            require('../assets/weather-icons/sunny.png')
                        }
                        style={{width: 50, height: 50}}
                    />
                    <Text style={[style.white, style.bold]}>
                        {this.timestampToDay(item.dt)}
                    </Text>
                    <Text>
                    {moment(item.dt * 1000).format('HH:mm')}
                    </Text>
                </View>
                <Text style={style.temp}>
                    {Math.round(item.main.temp)} °C
                </Text>
            </View>
        </FadeInView>

        return (
            <FlatList 
                keyExtractor={item=>item.dt.toString()} 
                data={this.filterArray()} 
                renderItem={this.props.forecast === 'forecast' ? forecast : daily}
            />
        )
    }
}

const style = StyleSheet.create({
    white: {
        color: '#FFF'
    },
    bold: {
        fontWeight: 'bold',
        marginHorizontal:5
    },
    firstView: {
        backgroundColor : '#e54b65',
    },
    bigText: {
        fontSize : 50,
    },
    view: {
        backgroundColor : '#394163',
        borderWidth: 1,
        borderBottomColor: 1,
        borderBottomColor: '#202340',
        paddingHorizontal: 20,
        paddingVertical: 10,
        flex:1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:'center'
    },
    temp: {
        color: '#FFF',
        fontWeight: 'bold',
        fontSize: 22
    }
})