import React from 'react';
import { Text, View, ActivityIndicator } from 'react-native';
import globalTheme from '../style/GlobalTheme'
import axios from 'axios'
import WeatherList from './WeatherList'

export default class Result extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            city : this.props.route.params.city,
            country : this.props.route.params.country,
            report : null,
            checked : 'forecast',
            error: false,
            loading: true
        };
        setTimeout(() => {this.fetchWeather()}, 1000);
    }

    async fetchWeather() {
        try {
            const checked = await AsyncStorage.getItem('CHECKED');
            this.setState({checked});
          } catch (error) {
              this.setState({checked: 'forecast'});
              // Error retrieving data
          }
        axios.get(
                'http://api.openweathermap.org/data/2.5/forecast?q=' + 
                this.state.city +  
                (this.state.checked === 'daily' ? '&cnt=7' : '' ) +
                '&mode=json&units=metric&appid=9e930c366f06818fb6684b25030377ec'
            )
            .then((response) => {
                this.setState({loading: false})
                this.setState({report: response.data})
            }).catch((error) => {
                if (error.message == 'Request failed with status code 404') {
                    this.setState({loading: false})
                    this.setState({error: true})
                }
            })
    }

    render() {
        if (this.state.loading == true) {
            return (
                <ActivityIndicator 
                    color={globalTheme.button.backgroundColor}
                    size="large"/>
            )
        } else if (this.state.report) {
            return (
                <View>
                    <WeatherList city={this.state.city} navigator={this.props.navigation} forecast={this.state.checked} data={this.state.report.list} />
                </View>
            )
        } else {
            return (
                <View>
                    <Text style={globalTheme.error}>*No city named {this.state.city} found*</Text>
                </View>
            )
        }   
    }
}