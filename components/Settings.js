import React from 'react';
import { View, Text, TextInput, Button, AsyncStorage, ToastAndroid } from 'react-native';
import { RadioButton } from 'react-native-paper';
import globalTheme from '../style/GlobalTheme'

export default class Settings extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            city: '',
            name: '',
            checked: 'daily'
        }
    }

    setName(name) {
        this.setState({name})
    }

    setCity(city) {
        this.setState({city})
    }

    async submit() {
        if (this.state.name && this.state.city) {
            try {
                await AsyncStorage.setItem('CITY', this.state.city);
                await AsyncStorage.setItem('NAME', this.state.name);
                await AsyncStorage.setItem('CHECKED', this.state.checked);
            } catch (error) {
                // Error saving data
            }
            ToastAndroid.show('Preferencies are set', ToastAndroid.SHORT);
        } else {
            ToastAndroid.show('Name and city fields can\'t be empty', ToastAndroid.SHORT);

        }
    }
    
    async clear() {
        try {
            AsyncStorage.removeItem('CITY');
            AsyncStorage.removeItem('NAME');
            AsyncStorage.removeItem('CHECKED');
            this.setState({city: '', name: '', checked: 'daily'})
          } catch (error) {
            // Error saving data
          }
    }
    
    async componentDidMount() {
        try {
            const city = await AsyncStorage.getItem('CITY');
            const name = await AsyncStorage.getItem('NAME');
            const checked = await AsyncStorage.getItem('CHECKED');
            this.setState({city, name, checked});
          } catch (error) {
              this.setState({city: '', name: '', checked: 'daily'});
              // Error retrieving data
          }
    }

    render() {
        return (
            <View style={globalTheme.container}>
                <Text style={globalTheme.title}>User Settings</Text>
                <Text>User name</Text>
                <TextInput
                    style={globalTheme.input}
                    onChangeText={(text) => this.setName(text)}
                    onSubmitEditing={() => this.submit()}
                    value={this.state.name}
                />  
                <Text>Favorite city</Text>
                <TextInput
                    style={globalTheme.input}
                    onChangeText={(text) => this.setCity(text)}
                    onSubmitEditing={() => this.submit()}
                    value={this.state.city}
                />
                <View style={globalTheme.flexRow}>
                    <RadioButton
                    color={globalTheme.button.backgroundColor}
                    value="daily"
                    status={this.state.checked === 'daily' ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ checked: 'daily' }); }}

                    />
                    <Text>Today detailled forecast</Text>
                </View>
                <View style={globalTheme.flexRow}>
                    <RadioButton
                    color={globalTheme.button.backgroundColor}
                    value="forecast"
                    status={this.state.checked === 'forecast' ? 'checked' : 'unchecked'}
                    onPress={() => { this.setState({ checked: 'forecast' }); }}
                    />
                    <Text>Incoming days forecast</Text>
                </View>
                <View style={[globalTheme.flexRow, globalTheme.justify]}>
                    <Button color={globalTheme.button.backgroundColor}  onPress={() => this.submit()} title="Save infos"/>
                    <Button color={globalTheme.button.backgroundColor}  onPress={() => this.clear()} title="Reset infos"/>
                </View>
            </View>
        )
    }
}