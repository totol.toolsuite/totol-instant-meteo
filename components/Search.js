import React from 'react';
import {TextInput, View, Button, Text} from 'react-native';
import globalTheme from '../style/GlobalTheme'
import massiveCityArray from '../utils/worldCities'
import famousCities from '../utils/famousCities'
import _ from 'lodash'

export default class Search extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            city: '',
            searchType: 'classic',
            advancedCountry: '',
            advancedCity: '',
            currentArray: famousCities,
            error: ''
        }
    }
    
    async loadFromPrefs() {
        try {
            const city = await AsyncStorage.getItem('CITY');
            this.setState({city})
          } catch (error) {
            this.setState({city: ''})
          }
    }

    setCity = (city) => this.setState({city})
    setAdvancedCountry = (advancedCountry) => this.setState({advancedCountry})
    setAdvancedCity = (advancedCity) => this.setState({advancedCity})

    submitSearch() {
        this.setState({error: ''})
        if (!this.state.advancedCountry && ! this.state.advancedCity) {
            this.setState({currentArray: famousCities});
        } else if (this.state.advancedCountry) {
            if (massiveCityArray.hasOwnProperty(this.state.advancedCountry)) {
                if (!this.state.advancedCity) {
                    let array = massiveCityArray[this.state.advancedCountry].slice(0,20);
                    this.setState({currentArray: array})
                } else {
                    let array = massiveCityArray[this.state.advancedCountry]
                    array = _.filter(array, x => x.indexOf(this.state.advancedCity) !== -1).slice(0,20)
                    if (array.length) {
                        this.setState({currentArray: array})
                    } else {
                        this.setState({error: 'City matching ' + this.state.advancedCity + ' not found', currentArray: []})
                    }
                }
            } else {
                this.setState({error: 'Country ' + this.state.advancedCountry + ' not found', currentArray: []})
            }
        } else {
            let array = [];
            _.forIn(massiveCityArray, (value, key) => {
                let tempArray = [];
                tempArray = _.filter(massiveCityArray[key], x => x.indexOf(this.state.advancedCity) !== -1).slice(0,20)
                tempArray = _.map(tempArray, x => x + '('+ key +')')
                tempArray.length ? array.push(tempArray) : ''
            })
            array = _.flatten(array).slice(0,20)
            if (array.length) {
                this.setState({currentArray: array})
            } else {
                this.setState({error: 'City matching ' + this.state.advancedCity + ' not found', currentArray: []})
            }
        }
    }

    submit(cityButton) {
        if (cityButton) {
            var exp=new RegExp(/(\(.*\))/g );
            var countryArray=cityButton.match(exp);
            if (countryArray && countryArray.length) {
                var country = countryArray[0].replace('(','').replace(')','')
                var city = cityButton.slice(0, cityButton.indexOf('('))
                this.props.navigation.navigate('Result', {city, country})
            } else {
                this.props.navigation.navigate('Result', {city: cityButton, country: ''})
            }
        } else {
            this.props.navigation.navigate('Result', {city: this.state.city})
        }
    }

    componentDidMount() {
        this.loadFromPrefs();
    }

    render() {
        const cityArray = 
            this.state.currentArray.map((x,i)=>
            <View 
                key={i}
                style={{margin: 5}}>
                <Button
                    style={{width: "80%"}}
                    color={globalTheme.buttonSecondary.backgroundColor}
                    onPress={() => this.submit(x)}
                    title={x}/>
            </View>)
        const classicMode = 
        <View>
            <TextInput
                style={globalTheme.input}
                onChangeText={(text) => this.setCity(text)}
                onSubmitEditing={() => this.submit()}
                value={this.state.city}
            />  
            <Button color={globalTheme.button.backgroundColor}  onPress={() => this.submit()} title="Search a city"/>
        </View>

        const advancedMode = 
        <View>
            <Text>Country</Text>
            <View style={[globalTheme.flexRow, globalTheme.justify]}>
                <TextInput
                    style={[{marginTop: 20, minWidth: '80%'},globalTheme.input]}
                    onChangeText={(text) => this.setAdvancedCountry(text)}
                    onSubmitEditing={() => this.submitSearch()}
                    value={this.state.advancedCountry}
                />  
                <Button color={globalTheme.button.backgroundColor}  onPress={() => this.submitSearch()} title="Filter"/>
            </View>
            <Text>City</Text>
            <View style={[globalTheme.flexRow, globalTheme.justify, {marginBottom: 20}]}>
                <TextInput
                    style={[{marginTop: 20, minWidth: '80%'},globalTheme.input]}
                    onChangeText={(text) => this.setAdvancedCity(text)}
                    onSubmitEditing={() => this.submitSearch()}
                    value={this.state.advancedCity}
                />  
                <Button color={globalTheme.button.backgroundColor}  onPress={() => this.submitSearch()} title="Filter"/>
            </View>
        </View>
        const error = <Text style={globalTheme.error}>{this.state.error}</Text>
        return (
            <View style={globalTheme.container}>
                <View style={[globalTheme.flexRow, globalTheme.justify, {marginBottom: 20}]}>
                    <Button 
                        color={this.state.searchType === 'classic' ? globalTheme.button.backgroundColor : globalTheme.buttonDisabled.backgroundColor}  
                        onPress={() => this.setState({searchType: 'classic'})} 
                        title="Classic search"/>
                    <Button 
                        color={this.state.searchType === 'advanced' ? globalTheme.button.backgroundColor : globalTheme.buttonDisabled.backgroundColor}  
                        onPress={() => this.setState({searchType: 'advanced'})} 
                        title="Advanced search"/>
                </View>
                {this.state.searchType === 'classic' ? classicMode : advancedMode}
                {error}
                <View style={{flexWrap: 'wrap', flexDirection: 'row', justifyContent: "center"}}>
                    {this.state.searchType === 'classic' ? <Text/> : cityArray}
                </View>
            </View>
        )
    }
}