export default {
    button: {
        backgroundColor: '#A2273C',
    },
    buttonSecondary: {
        backgroundColor: '#394163'
    },
    buttonDisabled: {
        backgroundColor: '#a3a3a3',
    },
    container: {
        margin: 20
    },
    title: {
        fontSize: 22,
        marginBottom: 20
    },
    input: {
        paddingHorizontal: 10, 
        marginBottom: 20, 
        height: 40, 
        borderColor: 'gray', 
        borderWidth: 1 
    },
    flexRow: {
        flexDirection: 'row',
        alignItems:'center'
    },
    center: {
        justifyContent: 'center'
    },
    justify: {
        justifyContent: 'space-between'
    },
    error: {
        color: 'red',
        fontSize: 14,
    }
};