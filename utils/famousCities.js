export default 
    [
        'Nantes',
        'Paris',
        'Bangkok',
        'London',
        'Dubai',
        'Kuala Lumpur',
        'Tokyo',
        'Istanbul',
        'Seoul',
        'Antalya',
        'Phuket',
        'Mecca',
        'Hong Kong',
        'Milan',
        'Palma de Mallorca',
        'Barcelona',
        'Pattaya',
        'Osaka',
        'Bali'
    ]