import React from 'react';
import { StatusBar, Text, View } from 'react-native';
import Settings from './components/Settings'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { NavigationContainer } from '@react-navigation/native';
import { MaterialCommunityIcons } from 'react-native-vector-icons';
import globalTheme from './style/GlobalTheme'
import Weather from './components/Weather';
import Home from './components/Home';

export default function App() {
  
  return (
    <NavigationContainer>
      <StatusBar hidden={true}/>
      <Tab.Navigator
        tabBarPosition={"bottom"}
        tabBarOptions={{
          style: {
            backgroundColor:globalTheme.button.backgroundColor,
            borderTopWidth: 2,
            borderColor: "#3f101c"
          },
          showIcon: true,
          showLabel: false,
          activeTintColor:'yellow',
          inactiveTintColor:'white',
          indicatorStyle: {
            height: 2,
            backgroundColor: '#FFF'
          }
        }}>
        <Tab.Screen 
          name="Home" 
          component={Home} 
          options={{
            test:'toto',
            tabBarLabel: "Home",
            tabBarIcon: ({ color, focused }) => 
              <MaterialCommunityIcons name="home" color={color} size={focused ? 20: 15}/>
          }}
        />
        <Tab.Screen 
          name="Search" 
          component={Weather} 
          options={{
            tabBarLabel: "Search",
            tabBarIcon: ({ color, focused }) => 
              <MaterialCommunityIcons name="loupe" color={color} size={focused ? 20: 15}/>
          }}
        />
        <Tab.Screen 
          name="Settings" 
          component={Settings} 
          options={{ 
            tabBarOnPress: ({navigation, defaultHandler}) => {console.log(navigation, defaultHandler)},
            tabBarLabel: 'Settings',
            tabBarIcon: ({ color, focused }) => 
              <MaterialCommunityIcons name="settings" color={color} size={focused ? 20: 15}/>
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}
const Tab = createMaterialTopTabNavigator();